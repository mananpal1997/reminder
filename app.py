from flask import Flask, Response, request, redirect, render_template, session, url_for
from twilio.rest import Client
from credentials import SID, TOKEN, API_KEY, SECRET_KEY
from authy.api import AuthyApiClient
import threading
import pytz
import phonenumbers
import datetime
import time

app = Flask(__name__)
app.secret_key = SECRET_KEY

api = AuthyApiClient(API_KEY)


@app.route("/")
def home():
    return render_template("service.html")


@app.route("/phone_verification", methods=["GET", "POST"])
def phone_verification():
    if request.method == "POST":
        country_code = request.form.get("country_code")
        phone_number = request.form.get("phone_number")
        method = request.form.get("method")

        session['country_code'] = country_code
        session['phone_number'] = phone_number

        api.phones.verification_start(phone_number, country_code, via=method)

        return redirect(url_for("verify"))

    return render_template("phone_verification.html")


@app.route("/verify", methods=["GET", "POST"])
def verify():
    if request.method == "POST":
        token = request.form.get("token")

        phone_number = session.get("phone_number")
        country_code = session.get("country_code")

        verification = api.phones.verification_check(phone_number, country_code, token)

        if verification.ok():
            t = threading.Thread(target=sms_scheduler, args=(phone_number, country_code, ))
            t.setDaemon(False)
            t.start()

            return Response("<h1>Success!</h1>")

        return Response("<h1>Failed!</h1>")

    return render_template("verify.html")


@app.route("/service", methods=["GET", "POST"])
def start_scheduler():
    if request.method == "POST":
    	country_code = request.form.get("country_code")
    	phone_number = request.form.get("phone_number")
    	print(phone_number, country_code)
    	t = threading.Thread(target=sms_scheduler, args=(phone_number, country_code, ))
    	t.setDaemon(False)
    	t.start()

    	return Response("<h1>Happy to help you! :)</h1>")

    return render_template("service.html")


def sms_scheduler(number, code):
    timezone = pytz.timezone(pytz.country_timezones(phonenumbers.region_code_for_country_code(int(code)))[0])
    cur_t = datetime.datetime.now(timezone)

    f = open("info.log", "w")
    f.write("[%s] Starting the service..." % cur_t.ctime())
    err_count = 0

    while True:
        try:
            cur_t = datetime.datetime.now(timezone)
            if 0 <= cur_t.hour <= 6 or cur_t.hour >= 22:
                # night time
                continue

            for n_try in range(7):  # 1 for ok, 5 for retries
                x = client.messages.create(
                    to="+%s%s" % (code, number),
                    from_="+16027865035",
                    body="Reminder! Take your medicine."
                )
                time.sleep(10)

                m = client.messages(x.sid).fetch()
                if m.error_message is None:
                    break
                else:
                    if n_try == 6:
                        f.write("[%s] SMS service not working. Shutting down the service..." % cur_t.ctime())
                        f.close()
                        break
                    else:
                        f.write("[%s] Failed to send message. Retrying..." % cur_t.ctime())

            err_count = 0
            time.sleep(3600)
        except Exception as err:
            cur_t = datetime.datetime.now(timezone)
            f.write("[%s] Service failed.\n<ERROR> : %s" % (cur_t.ctime(), err))
            err_count += 1
            if err_count == 5:
                f.close()
                break


if __name__ == '__main__':
    account_sid = SID
    auth_token = TOKEN

    client = Client(account_sid, auth_token)
    app.run(debug=True)
